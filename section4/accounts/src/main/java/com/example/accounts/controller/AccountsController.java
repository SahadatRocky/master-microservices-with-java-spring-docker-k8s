package com.example.accounts.controller;

import com.example.accounts.model.Account;
import com.example.accounts.repository.AccountsRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AccountsController {
    private final AccountsRepository accountsRepository;
    public AccountsController(AccountsRepository accountsRepository){
        this.accountsRepository = accountsRepository;
    }

    @GetMapping("/myAccount/{id}")
    public Account getAccountDetails(@PathVariable("id") Integer id) {

        Account accounts = accountsRepository.findByCustomerId(id);
        if (accounts != null) {
            return accounts;
        } else {
            return null;
        }
    }
}
