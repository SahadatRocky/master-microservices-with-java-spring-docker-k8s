/**
 * 
 */
package com.example.cards.controller;

import java.util.List;

import com.example.cards.model.Card;
import com.example.cards.repository.CardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
public class CardController {

	@Autowired
	private CardRepository cardRepository;

	@GetMapping("/myCard/{customerId}")
	public List<Card> getCardDetails(@PathVariable("customerId") Integer customerId) {
		List<Card> cards = cardRepository.findByCustomerId(customerId);
		if (cards != null) {
			return cards;
		} else {
			return null;
		}

	}

}
