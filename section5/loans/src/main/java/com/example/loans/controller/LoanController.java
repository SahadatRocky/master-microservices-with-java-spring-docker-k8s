/**
 * 
 */
package com.example.loans.controller;

import java.util.List;

import jakarta.persistence.criteria.CriteriaBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.example.loans.model.Customer;
import com.example.loans.model.Loan;
import com.example.loans.repository.LoanRepository;

@RestController
public class LoanController {

	@Autowired
	private LoanRepository loanRepository;

	@GetMapping("/myLoan/{customerId}")
	public List<Loan> getLoansDetails(@PathVariable("customerId") Integer customerId) {
		List<Loan> loans = loanRepository.findByCustomerIdOrderByStartDtDesc(customerId);
		if (loans != null) {
			return loans;
		} else {
			return null;
		}

	}

}
