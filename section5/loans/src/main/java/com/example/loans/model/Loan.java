package com.example.loans.model;

import java.sql.Date;

import jakarta.persistence.*;

import lombok.Data;

@Data
@Entity
@Table(name="loan")

public class Loan {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "loan_number")
	private Integer loanNumber;
	
	@Column(name = "customer_id")
	private Integer customerId;
	
	@Column(name="start_dt")
	private Date startDt;
	
	@Column(name = "loan_type")
	private String loanType;
	
	@Column(name = "total_loan")
	private Integer totalLoan;
	
	@Column(name = "amount_paid")
	private Integer amountPaid;
	
	@Column(name = "outstanding_amount")
	private Integer outstandingAmount;
	
	@Column(name = "create_dt")
	private String createDt;
	
}
