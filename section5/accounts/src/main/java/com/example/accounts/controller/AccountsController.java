package com.example.accounts.controller;

import com.example.accounts.model.Account;
import com.example.accounts.repository.AccountsRepository;
import com.example.accounts.service.MyApiService;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
public class AccountsController {


    private final AccountsRepository accountsRepository;
    private final MyApiService apiService;

    public AccountsController(AccountsRepository accountsRepository,
                              MyApiService apiService){
        this.accountsRepository = accountsRepository;
        this.apiService = apiService;
    }

    @GetMapping("/myAccount/{id}")
    public Account getAccountDetails(@PathVariable("id") Integer id) {

        Account accounts = accountsRepository.findByCustomerId(id);
        if (accounts != null) {
            return accounts;
        } else {
            return null;
        }
    }

    @GetMapping("/get-address/{latitude}/{longitude}")
    public Mono<JsonNode> fetchData(@PathVariable("latitude") String latitude, @PathVariable("longitude") String longitude ) {
        return apiService.fetchDataFromApi(latitude,longitude);
    }
}
