/**
 * 
 */
package com.example.cards.controller;

import java.util.List;

import com.example.cards.config.CardsServiceConfig;
import com.example.cards.model.Card;
import com.example.cards.model.Properties;
import com.example.cards.repository.CardRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/cards")
public class CardController {
	private static final Logger LOGGER
			= LoggerFactory.getLogger(CardController.class);
	@Autowired
	private CardRepository cardRepository;

	@Autowired
	private CardsServiceConfig cardsConfig;

	@GetMapping("/myCard/{customerId}")
	public List<Card> getCardDetails(@PathVariable("customerId") Integer customerId) {
		LOGGER.info("getCardDetails() method start");
		List<Card> cards = cardRepository.findByCustomerId(customerId);
		if (cards != null) {
			return cards;
		} else {
			return null;
		}
	}

	@GetMapping("/card/properties")
	public String getPropertyDetails() throws JsonProcessingException {
		ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
		Properties properties = new Properties(cardsConfig.getMsg(), cardsConfig.getBuildVersion(),
				cardsConfig.getMailDetails(), cardsConfig.getActiveBranches());
		String jsonStr = ow.writeValueAsString(properties);
		return jsonStr;
	}

}
