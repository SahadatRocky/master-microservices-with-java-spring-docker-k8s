package com.example.gatewayserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.cors.reactive.CorsWebFilter;
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource;

import java.util.Arrays;
import java.util.List;

@SpringBootApplication
@EnableDiscoveryClient
public class GatewayserverApplication {

	public static void main(String[] args) {
		SpringApplication.run(GatewayserverApplication.class, args);
	}

//	@Bean
//	public CorsWebFilter corsFilter() {
//		org.springframework.web.cors.CorsConfiguration corsConfiguration = new org.springframework.web.cors.CorsConfiguration();
//		corsConfiguration.setAllowCredentials(false);
//		corsConfiguration.addAllowedOrigin("*");
//		corsConfiguration.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "DELETE", "OPTIONS", "HEAD"));
//		corsConfiguration.addAllowedHeader("origin");
//		corsConfiguration.addAllowedHeader("content-type");
//		corsConfiguration.addAllowedHeader("accept");
//		corsConfiguration.addAllowedHeader("authorization");
//		corsConfiguration.addAllowedHeader("cookie");
//		corsConfiguration.setAllowedOriginPatterns(List.of("*"));
//		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
//		source.registerCorsConfiguration("/**", corsConfiguration);
//		return new CorsWebFilter(source);
//	}
}


