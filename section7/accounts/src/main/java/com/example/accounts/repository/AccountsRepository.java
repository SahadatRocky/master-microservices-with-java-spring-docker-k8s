package com.example.accounts.repository;

import com.example.accounts.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountsRepository extends JpaRepository<Account,Integer> {
    Account findByCustomerId(int customerId);
}
