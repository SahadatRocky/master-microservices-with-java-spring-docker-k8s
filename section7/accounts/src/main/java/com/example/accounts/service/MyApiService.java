package com.example.accounts.service;

import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Service
public class MyApiService {

    private final WebClient webClient;

    @Autowired
    public MyApiService(WebClient.Builder webClientBuilder) {
        this.webClient = webClientBuilder.baseUrl("https://api.example.com").build();
    }

    public Mono<JsonNode> fetchDataFromApi(String latitude, String longitude) {
        String url = "https://nominatim.openstreetmap.org/search.php?q="+latitude+","+longitude+"&polygon_geojson=1&format=json";
        Mono<JsonNode> jsonNode = webClient
                .get()
                .uri(url)
                .retrieve()
                .bodyToMono(JsonNode.class);
        System.out.println("jsonNode::"+jsonNode);
        return jsonNode;
    }
}
