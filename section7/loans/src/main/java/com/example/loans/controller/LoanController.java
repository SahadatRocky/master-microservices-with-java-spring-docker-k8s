/**
 * 
 */
package com.example.loans.controller;

import java.util.List;

import com.example.loans.config.LoansServiceConfig;
import com.example.loans.model.Properties;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import jakarta.persistence.criteria.CriteriaBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.example.loans.model.Customer;
import com.example.loans.model.Loan;
import com.example.loans.repository.LoanRepository;

@RestController
public class LoanController {

	@Autowired
	private LoansServiceConfig loansConfig;
	@Autowired
	private LoanRepository loanRepository;

	@GetMapping("/myLoan/{customerId}")
	public List<Loan> getLoansDetails(@PathVariable("customerId") Integer customerId) {
		List<Loan> loans = loanRepository.findByCustomerIdOrderByStartDtDesc(customerId);
		if (loans != null) {
			return loans;
		} else {
			return null;
		}
	}

	@GetMapping("/loans/properties")
	public String getPropertyDetails() throws JsonProcessingException {
		ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
		Properties properties = new Properties(loansConfig.getMsg(), loansConfig.getBuildVersion(),
				loansConfig.getMailDetails(), loansConfig.getActiveBranches());
		String jsonStr = ow.writeValueAsString(properties);
		return jsonStr;
	}

}
