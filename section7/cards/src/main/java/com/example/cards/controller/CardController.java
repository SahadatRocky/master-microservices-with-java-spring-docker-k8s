/**
 * 
 */
package com.example.cards.controller;

import java.util.List;

import com.example.cards.config.CardsServiceConfig;
import com.example.cards.model.Card;
import com.example.cards.model.Properties;
import com.example.cards.repository.CardRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
public class CardController {

	@Autowired
	private CardRepository cardRepository;

	@Autowired
	private CardsServiceConfig cardsConfig;

	@GetMapping("/myCard/{customerId}")
	public List<Card> getCardDetails(@PathVariable("customerId") Integer customerId) {
		List<Card> cards = cardRepository.findByCustomerId(customerId);
		if (cards != null) {
			return cards;
		} else {
			return null;
		}
	}

	@GetMapping("/cards/properties")
	public String getPropertyDetails() throws JsonProcessingException {
		ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
		Properties properties = new Properties(cardsConfig.getMsg(), cardsConfig.getBuildVersion(),
				cardsConfig.getMailDetails(), cardsConfig.getActiveBranches());
		String jsonStr = ow.writeValueAsString(properties);
		return jsonStr;
	}

}
