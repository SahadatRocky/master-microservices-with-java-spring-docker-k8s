package com.example.springjpa.exception;

import java.util.Locale;

public class ExceptionMessageText {
    public static final String INVALID_TOKEN = "Invalid Token";
    public static final String INVALID_TOKEN_BN = "ইনভ্যালিড টোকেন";
    public static final String PASSWORD_MISMATCH = "Password Mismatch";
    public static final String PASSWORD_MISMATCH_BN = "পাসওয়ার্ড মিসমাচ";
    public static final String NO_USER_FOUND_EN = "User not found";
    public static final String NO_USER_FOUND_BN = "ব্যবহারকারী পাওয়া যায়নি";
    public static final String UNREGISTER_USER_MSG_EN = "You are not registered yet, to register please click the register button";
    public static final String UNREGISTER_USER_MSG_BN = "আপনি এখনও রেজিস্টার্ড নন, রেজিস্ট্রেশন করতে রেজিস্টার বাঁটনে ক্লিক করুন";
    public static final String USER_TYPE_NOT_FOUND_EXCEPTION_EN = "Type mismatch";
    public static final String USER_TYPE_NOT_FOUND_EXCEPTION_BN = "টাইপ মিসমাচ";

    public static final String REFRESH_NOT_FOUND_MSG_EN = "Refresh token is not in database!";
    public static final String REFRESH_NOT_FOUND_MSG_BN = "রিফ্রেশ টোকেন ডাটাবেসে নেই!";

    public static final String REFRESH_TOKEN_EXPIRE_EN = "Your refresh token was expired. Please make a new login request";
    public static final String REFRESH_TOKEN_EXPIRE_BN = "আপনার রিফ্রেশ টোকেন মেয়াদ শেষ হয়েছে. একটি নতুন লগইন অনুরোধ করুন";

    public static final String NOT_FOUND_EXCEPTION_EN = "Type not found by : ";
    public static final String NOT_FOUND_EXCEPTION_BN = "টাইপ পাওয়া যায়নি:";

    public static final String DISTRICT_ID_NOT_FOUND_EX_EN = "District not found by id :";
    public static final String DISTRICT_ID_NOT_FOUND_EX_BN = "জেলা পাওয়া যায়নি:";

    public static final String THANA_ID_NOT_FOUND_EX_EN = "Thana not found by id :";
    public static final String THANA_ID_NOT_FOUND_EX_BN = "থানা পাওয়া যায়নি:";

    public static final String PRODUCT_NOT_FOUND_IN_CART_MSG_EN = "Product not found in cart.";
    public static final String PRODUCT_NOT_FOUND_IN_CART_MSG_BN = "পণ্য কার্টে পাওয়া যায়নি।";

    public static final String PRODUCT_NOT_FOUND_MSG_EN = "Product not found.";
    public static final String PRODUCT_NOT_FOUND_MSG_BN = "পণ্য পাওয়া যায়নি।";

    public static final String COUPON_NOT_FOUND_MSG_EN = "Coupon not found by code:";
    public static final String COUPON_NOT_FOUND_MSG_BN = "কুপন পাওয়া যায়নি:";

    public static final String COUPON_NOT_FOUND_BY_ID_MSG_EN = "Coupon not found by id:";
    public static final String INVALID_COUPON_CODE_MSG_EN = "Invalid coupon code";

    public static final String COUPON_NOT_FOUND_BY_ID_MSG_BN = "কোড দ্বারা কুপন পাওয়া যায়নি:";

    public static final String COUPON_ALREADY_USED_MSG_EN = "Coupon has already been used! code:";
    public static final String COUPON_ALREADY_USED_MSG_BN = "কুপন ইতিমধ্যে ব্যবহার করা হয়েছে! কোড:";

    public static final String COUPON_VALIDATION_MSG_EN = "Invalid coupon! code:";
    public static final String COUPON_VALIDATION_MSG_BN = "ইনভ্যালিড কুপন ! কোড:";

    public static final String COUPON_ALREADY_EXPIRED_MSG_EN = "Coupon already expired! code:";
    public static final String COUPON_ALREADY_EXPIRED_MSG_BN = "কুপন ইতিমধ্যে মেয়াদ শেষ! কোড:";

    public static final String ID_NOT_FOUND_MSG_EN = "Id not found";
    public static final String ID_NOT_FOUND_MSG_BN = "আইডি পাওয়া যায়নি";

    public static final String CANCEL_REASON_LIST_NOT_FOUND_MSG_EN = "Cancel reason not found";
    public static final String CANCEL_REASON_LIST_NOT_FOUND_MSG_BN = "ক্যানসেল কারণ খুঁজে পাওয়া যায়নি";

    public static final String PARTNER_INFO_NOT_FOUND_MSG_EN = "Partner info not found";
    public static final String PARTNER_INFO_NOT_FOUND_MSG_BN = "পার্টনার তথ্য পাওয়া যায়নি";

    public static final String SEARCH_TEXT_ERROR_MSG_EN = "Search text minimum 2 character";
    public static final String SEARCH_TEXT_ERROR_MSG_BN = "সার্চ টেক্সট ন্যূনতম 2 অক্ষর";

    public static final String SERVICE_DISCOUNT_NOT_FOUND_MSG_EN = "Service discount not found by id:";
    public static final String SERVICE_DISCOUNT_NOT_FOUND_MSG_BN = "আইডি দ্বারা সার্ভিস ডিসকাউন্ট পাওয়া যায়নি:";

    public static final String BRAND_IMAGE_NOT_FOUND_BY_ID_MSG_EN = "Brand-Image not found by id:";
    public static final String BRAND_IMAGE_NOT_FOUND_BY_ID_MSG_BN = "ব্রান্ড-ইমেজ আইডি দ্বারা পাওয়া যায়নি:";

    public static final String MENU_NOT_FOUND_MSG_EN = "Menu not found";
    public static final String MENU_NOT_FOUND_MSG_BN = "মেনু পাওয়া যায়নি";

    public static final String SLIDER_IMAGE_NOT_FOUND_MSG_EN = "Slider-image not found by id:";
    public static final String SLIDER_IMAGE_NOT_FOUND_MSG_BN = "স্লাইডার-ইমেজ আইডি দ্বারা পাওয়া যায়নি:";
    public static final String CHECK_PASSWORD_MSG_EN = "Please check password";
    public static final String CHECK_PASSWORD_MSG_BN = "পাসওয়ার্ড চেক করুন";
    public static final String USER_NOT_FOUND_MSG_EN = "User not found";
    public static final String USER_NOT_FOUND_MSG_BN = "ব্যবহারকারী পাওয়া যায়নি";
    public static final String USER_NOT_FOUND_BY_ID_MSG_EN = "User not found by id: ";
    public static final String USER_NOT_FOUND_BY_ID_MSG_BN = "ব্যবহারকারী পাওয়া যায়নি: ";

    public static final String CHECK_CURRENT_PASSWORD_MSG_EN = "Please check the current password";
    public static final String CHECK_CURRENT_PASSWORD_MSG_BN = "বর্তমান পাসওয়ার্ড চেক করুন";

    public static final String PHONE_VALIDATION_MSG_EN = " : phone is already in use";
    public static final String PHONE_VALIDATION_MSG_BN = " : ফোন ইতিমধ্যে ব্যবহার করা হয়েছে।";

    public static final String EMAIL_VALIDATION_MSG_EN = " : email is already in use";
    public static final String EMAIL_VALIDATION_MSG_BN = " : ইমেইল ইতিমধ্যে ব্যবহার করা  হয়েছে। ";

    public static final String OTP_NOT_FOUND_MSG_EN = "OTP not found";
    public static final String OTP_NOT_FOUND_MSG_BN = "ও টি পি পাওয়া যায়নি";

    public static final String ENTERED_WRONG_OTP_MSG_EN = "You have entered the wrong OTP";
    public static final String ENTERED_WRONG_OTP_MSG_BN = "আপনি ভুল ওটিপি প্রবেশ করেছেন";

    public static final String ADD_CART_VALIDATION_MSG_EN = "You can add maximum two cylinders in a single order.";
    public static final String ADD_CART_VALIDATION_MSG_BN = "আপনি একক অর্ডারে সর্বাধিক দুটি সিলিন্ডার যোগ করতে পারেন।";

    public static String getNoUserFoundErrorMsg(Locale locale, String userId) {
        if (locale == null) {
            return "User not found by id [" + userId + "]";
        }
        if (locale.getLanguage().equals("en")) {
            return "User not found by id [" + userId + "]";
        } else {
            return "ব্যবহারকারী পাওয়া যায়নি [" + userId + "]";
        }

    }

}
