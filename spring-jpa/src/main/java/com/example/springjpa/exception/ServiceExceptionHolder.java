package com.example.springjpa.exception;

import lombok.Getter;

public class ServiceExceptionHolder {

    public final static int EXCEPTION_ID_NOT_FOUND_IN_DB = 1001;
    public final static int PASSWORD_MISMATCH_EXCEPTION_CODE = 1002;
    public final static int SPRING_SECURITY_EXCEPTION = 9000;
    public final static int REFRESH_TOKEN_EXCEPTION = 9001;

    @Getter
    public static class ServiceException extends RuntimeException {
        private final int code;
        private final String message;
        private final String messageBn;

        public ServiceException(int code, String message, String messageBn) {
            this.code = code;
            this.message = message;
            this.messageBn = messageBn;
        }
    }

    public static class ResourceNotFoundException extends ServiceException {
        public ResourceNotFoundException(int code, String message, String messageBn) {
            super(code, message, messageBn);
        }
    }

    public static class ServerNotFoundException extends ResourceNotFoundException {
        public ServerNotFoundException(final String msg, final int code) {
            super(code, msg, null);
        }
    }

    public static class IdNotFoundInDBException extends ResourceNotFoundException {
        public IdNotFoundInDBException(final String msg, final String msgBn) {
            super(EXCEPTION_ID_NOT_FOUND_IN_DB, msg, msgBn);
        }
    }

    public static class RefreshTokenException extends ResourceNotFoundException {
        public RefreshTokenException(final String msg, final String msgBn) {
            super(REFRESH_TOKEN_EXCEPTION, msg, msgBn);
        }
    }


    public static class CustomException extends ResourceNotFoundException {
        public CustomException(int code, final String msg, final String msgBn) {
            super(code, msg, msgBn);
        }
    }
}
