package com.example.springjpa.resource;
import com.example.springjpa.beans.AuthorBean;
import com.example.springjpa.service.AuthorService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
public class AuthorResource {

    private final AuthorService authorService;

    @GetMapping("/author/get-all")
    public ResponseEntity<?> getAll(Pageable pageable){
        return new ResponseEntity<>(authorService.getAll(pageable),HttpStatus.OK);
    }

    @GetMapping("/author/get-by/{id}")
    public ResponseEntity<?> getById(@PathVariable("id") Integer id){
        return new ResponseEntity<>(authorService.getById(id), HttpStatus.OK);
    }

    @GetMapping("/author/dropdown-list")
    public ResponseEntity<?> getDropdownList(){
       return new ResponseEntity<>(authorService.getDropdownlist(), HttpStatus.OK);
    }

    @PostMapping("/author")
    public ResponseEntity<?> create(@RequestBody AuthorBean authorBean){
        return new ResponseEntity<>(authorService.create(authorBean), HttpStatus.OK);
    }

    @PutMapping("/author/{id}")
    public ResponseEntity<?> update(@PathVariable("id") Integer id, @RequestBody  AuthorBean authorBean) {
        return new ResponseEntity<>(authorService.update(id, authorBean), HttpStatus.OK);
    }

    @GetMapping("/authors")
    public ResponseEntity<?> getAuthorProjectionList(){
        return new ResponseEntity<>(authorService.getAuthorProjectionList(), HttpStatus.OK);
    }

    @DeleteMapping("/author/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") Integer id){
        return new ResponseEntity<>(authorService.delete(id), HttpStatus.OK);
    }






}
