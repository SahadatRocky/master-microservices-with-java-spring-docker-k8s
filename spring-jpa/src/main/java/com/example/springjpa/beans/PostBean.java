package com.example.springjpa.beans;
import lombok.Data;

@Data
public class PostBean {

    private  Integer Id;
    private String title;
    private String content;
    private AuthorBean author;

}
