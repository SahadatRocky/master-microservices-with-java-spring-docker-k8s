package com.example.springjpa.beans;
import lombok.Data;

@Data
public class AuthorBean {
    private  Integer Id;
    private String name;
}
