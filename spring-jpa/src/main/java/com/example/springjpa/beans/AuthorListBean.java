package com.example.springjpa.beans;
import lombok.Data;

@Data
public class AuthorListBean {
    private  Integer Id;
    private String name;
}
