package com.example.springjpa.service;
import com.example.springjpa.beans.*;
import com.example.springjpa.exception.ExceptionMessageText;
import com.example.springjpa.exception.ServiceExceptionHolder;
import com.example.springjpa.models.Author;
import com.example.springjpa.models.Post;
import com.example.springjpa.projection.PostProjection;
import com.example.springjpa.repository.PostRepository;
import com.example.springjpa.util.AppConstant;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PostService {
    private PostRepository postRepository;
    private ModelMapper modelMapper;
    public PostService(PostRepository postRepository,ModelMapper modelMapper){
        this.postRepository = postRepository;
        this.modelMapper = modelMapper;
    }

    public PostBean create(PostBean postBean) {
        Post post = modelMapper.map(postBean, Post.class);
        Post save = postRepository.save(post);
        return modelMapper.map(save, PostBean.class);
    }

    public List<PostProjection> getPostProjectionList() {
       return postRepository.findAllPost();
    }

    public Page<PostListBean> getAll(Pageable pageable) {

        PageRequest pageRequest = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), Sort.by(Sort.Order.desc(("id"))));
        Page<Post> page = postRepository.findAll(pageRequest);
        List<PostListBean> beans = page.getContent()
                .stream()
                .sorted(Comparator.comparing(Post::getId).reversed())
                .map(this::getPostInfoListBean)
                .collect(Collectors.toUnmodifiableList());
        return new PageImpl<>(beans, page.getPageable(), page.getTotalElements());

    }

    private PostListBean getPostInfoListBean(Post post) {
        PostListBean postListBean = modelMapper.map(post, PostListBean.class);
        return postListBean;
    }

    public PostDetailsBean getById(Integer id) {
        Optional<Post> optional = postRepository.findById(id);
        if(optional.isEmpty()){
            return null;
        }
        return getConvertBean(optional.get());
    }

    private PostDetailsBean getConvertBean(Post post) {
        return modelMapper.map(post, PostDetailsBean.class);
    }

    public List<DropdownDTO> getDropdownlist() {
        List<Post> list = postRepository.findAll(Sort.by("title")).stream().collect(Collectors.toUnmodifiableList());
        return list.stream().map(this:: getDropdownDto).collect(Collectors.toUnmodifiableList());
    }

    private DropdownDTO getDropdownDto(Post post) {
        DropdownDTO dropdownDTO = modelMapper.map(post, DropdownDTO.class);
        dropdownDTO.setValue(post.getTitle());
        return dropdownDTO;
    }

    public PostBean update(Integer id, PostBean postBean) {
        Post post = postRepository.findById(id).orElseThrow(() -> new ServiceExceptionHolder.CustomException(ServiceExceptionHolder.EXCEPTION_ID_NOT_FOUND_IN_DB,
                "Not Found with ID: " + id,
                "NOt Found with ID: " + id
        ));;
        BeanUtils.copyProperties(postBean, post, AppConstant.IGNORE_PROPERTIES);
        Post update = postRepository.save(post);
        return modelMapper.map(update, PostBean.class);
    }

    public Object delete(Integer id) {
        Post post = postRepository.findById(id).orElseThrow(() -> new ServiceExceptionHolder.CustomException(ServiceExceptionHolder.EXCEPTION_ID_NOT_FOUND_IN_DB,
                "Not Found with ID: " + id,
                "NOt Found with ID: " + id
        ));
        postRepository.delete(post);
        return Boolean.TRUE;
    }

    public List<PostDetailsBean> search(String searchText) {
        if(searchText.length() < 2){
            throw new ServiceExceptionHolder.CustomException(1002,
                    ExceptionMessageText.SEARCH_TEXT_ERROR_MSG_EN,
                    ExceptionMessageText.SEARCH_TEXT_ERROR_MSG_BN
            );
        }

        List<Integer> postIds = postRepository.search(searchText)
                .stream().map(Post:: getId)
                .collect(Collectors.toUnmodifiableList());

        return postRepository.findAllByPostIds(postIds)
                .stream()
                .sorted(Comparator.comparing(Post::getId).reversed())
                .map(this::getPostDetailsBean)
                .collect(Collectors.toUnmodifiableList());
    }

    private PostDetailsBean getPostDetailsBean(Post post) {
        PostDetailsBean postDetailsBean = modelMapper.map(post, PostDetailsBean.class);
        return postDetailsBean;
    }
}
