package com.example.springjpa.service;

import com.example.springjpa.beans.AuthorBean;
import com.example.springjpa.beans.AuthorDetailsBean;
import com.example.springjpa.beans.AuthorListBean;
import com.example.springjpa.beans.DropdownDTO;
import com.example.springjpa.exception.ServiceExceptionHolder;
import com.example.springjpa.models.Author;
import com.example.springjpa.projection.AuthorProjection;
import com.example.springjpa.repository.AuthorRepository;
import com.example.springjpa.util.AppConstant;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class AuthorService {

    private AuthorRepository authorRepository;
    private ModelMapper modelMapper;
    public AuthorService(AuthorRepository authorRepository,ModelMapper modelMapper){
         this.authorRepository = authorRepository;
         this.modelMapper = modelMapper;
    }

    public AuthorBean create(AuthorBean authorBean) {
        Author author = modelMapper.map(authorBean, Author.class);
        Author save = authorRepository.save(author);
        return modelMapper.map(save, AuthorBean.class);
    }

    public List<AuthorProjection> getAuthorProjectionList() {
        return authorRepository.findAllAuthor();
    }

    public Page<AuthorListBean> getAll(Pageable pageable) {
        PageRequest pageRequest = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), Sort.by(Sort.Order.desc("id")));
        Page<Author> page = authorRepository.findAll(pageRequest);
        List<AuthorListBean> beans = page.getContent()
                .stream()
                .sorted(Comparator.comparing(Author::getId).reversed())
                .map(this:: getAuthorInfoListBean).collect(Collectors.toUnmodifiableList());

        return new PageImpl<>(beans, page.getPageable(), page.getTotalElements());
    }

    private AuthorListBean getAuthorInfoListBean(Author author) {
        AuthorListBean bean = modelMapper.map(author, AuthorListBean.class);
        return bean;
    }

    public AuthorDetailsBean getById(Integer id) {
        Optional<Author> optional = authorRepository.findById(id);
        if(optional.isEmpty()){
            return null;
        }

        return getConvertBean(optional.get());
    }

    private AuthorDetailsBean getConvertBean(Author author) {
        AuthorDetailsBean bean = modelMapper.map(author, AuthorDetailsBean.class);
        return bean;
    }

    public List<DropdownDTO> getDropdownlist() {
       List<Author> list = authorRepository.findAll(Sort.by("name")).stream().collect(Collectors.toUnmodifiableList());
       return list.stream().map(this::getDropdownDto).collect(Collectors.toUnmodifiableList());
    }

    private DropdownDTO getDropdownDto(Author author) {
        DropdownDTO dropdownDTO = modelMapper.map(author, DropdownDTO.class);
        dropdownDTO.setValue(author.getName());
        return dropdownDTO;
    }

    public AuthorBean update(Integer id, AuthorBean authorBean) {
        Author author = authorRepository.findById(id).orElseThrow(() -> new ServiceExceptionHolder.CustomException(ServiceExceptionHolder.EXCEPTION_ID_NOT_FOUND_IN_DB,
                 "Not Found with ID: " + id,
                 "NOt Found with ID: " + id
        ));;
        BeanUtils.copyProperties(authorBean, author, AppConstant.IGNORE_PROPERTIES);
        Author update = authorRepository.save(author);
        return modelMapper.map(update, AuthorBean.class);
    }


    public Object delete(Integer id) {
        Author author = authorRepository.findById(id).orElseThrow(() -> new ServiceExceptionHolder.CustomException(ServiceExceptionHolder.EXCEPTION_ID_NOT_FOUND_IN_DB,
                "Not Found with ID: " + id,
                "NOt Found with ID: " + id
        ));
        authorRepository.delete(author);
        return Boolean.TRUE;
    }
}
