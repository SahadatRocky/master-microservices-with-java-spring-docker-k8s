package com.example.springjpa.util;

public class AppConstant {

    public final static String GET_LIST = "";
    public final static String CREATE = "";
    public final static String UPDATE = "/{id}";
    public final static String GET_BY_ID = "/{id}";
    public final static String GET_DROPDOWN_LIST = "dropdown-list";
    public static final String[] IGNORE_PROPERTIES =  {"id", "dateCreated"};
}
