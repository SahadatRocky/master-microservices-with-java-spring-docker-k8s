
version: "3.8"

services:
  mysqldb:
    container_name: mysqldb
    image: mysql
    environment:
      MYSQL_DATABASE: test_ms_cloud
      MYSQL_ROOT_PASSWORD: root
    ports:
      - 3307:3306
    volumes:
      - ./mysql-data:/var/lib/mysql
    networks:
      - service-mysql-net

  zipkin:
    image: openzipkin/zipkin
    container_name: zipkin
    ports:
      - 9411:9411
    networks:
      - service-config-net

  eurekaserver:
    container_name: eurekaserver
    build: ../../eurekaserver
    ports:
      - 8761:8761
    depends_on:
      - configserver
    environment:
      - spring.application.name=EUREKA-SERVICE
      - spring.profiles.active=default
      - eureka.client.registerWithEureka=false
      - eureka.client.fetchRegistry=false
      - spring.cloud.config.import=http://configserver:8071/
      - spring.cloud.config.import-check.enabled=false
      - management.zipkin.tracing.endpoint=http://zipkin:9411/api/v2/spans
    networks:
      - service-config-net
    deploy:
      restart_policy:
        condition: on-failure
        delay: 5s
        max_attempts: 3
        window: 120s

  configserver:
    container_name: configserver
    build: ../../configserver
    ports:
      - 8071:8071
    environment:
      - spring.application.name=CONFIG-SERVER
      - spring.cloud.config.server.git.uri=https://github.com/eazybytes/microservices-config.git
      - spring.cloud.config.server.git.clone-on-start=true
      - spring.cloud.config.server.git.default-label=main
      - eureka.client.registerWithEureka=true
      - eureka.client.fetchRegistry=true
      - eureka.client.serviceUrl.defaultZone=http://eurekaserver:8761/eureka
      - management.zipkin.tracing.endpoint=http://zipkin:9411/api/v2/spans
    networks:
      - service-config-net

  accounts-service:
    container_name: accounts-service
    build: ../../accounts
    ports:
      - 8091:8090
    depends_on:
      - mysqldb
      - configserver
      - eurekaserver
    environment:
      - spring.jpa.hibernate.ddl-auto=update
      - spring.datasource.url=jdbc:mysql://mysqldb:3306/test_ms_cloud
      - spring.datasource.username=root
      - spring.datasource.password=root
      - spring.datasource.driver-class-name=com.mysql.cj.jdbc.Driver
      - spring.application.name=ACCOUNTS-SERVICE
      - spring.profiles.active=default
      - spring.cloud.config.import=http://configserver:8071/
      - spring.cloud.config.import-check.enabled=false
      - eureka.client.registerWithEureka=true
      - eureka.client.fetchRegistry=true
      - eureka.client.serviceUrl.defaultZone=http://eurekaserver:8761/eureka/
      - management.zipkin.tracing.endpoint=http://zipkin:9411/api/v2/spans
    networks:
      - service-mysql-net
      - service-config-net
    deploy:
      restart_policy:
        condition: on-failure
        delay: 5s
        max_attempts: 3
        window: 120s

  cards-service:
    container_name: cards-service
    build: ../../cards
    ports:
      - 7091:7090
    depends_on:
      - mysqldb
      - configserver
      - eurekaserver
    environment:
      - spring.jpa.hibernate.ddl-auto=update
      - spring.datasource.url=jdbc:mysql://mysqldb:3306/test_ms_cloud
      - spring.datasource.username=root
      - spring.datasource.password=root
      - spring.datasource.driver-class-name=com.mysql.cj.jdbc.Driver
      - spring.application.name=CARDS-SERVICE
      - spring.profiles.active=default
      - spring.cloud.config.import=http://configserver:8071/
      - spring.cloud.config.import-check.enabled=false
      - eureka.client.registerWithEureka=true
      - eureka.client.fetchRegistry=true
      - eureka.client.serviceUrl.defaultZone=http://eurekaserver:8761/eureka/
      - management.zipkin.tracing.endpoint=http://zipkin:9411/api/v2/spans
    networks:
      - service-mysql-net
      - service-config-net
    deploy:
      restart_policy:
        condition: on-failure
        delay: 5s
        max_attempts: 3
        window: 120s

  loans-service:
    container_name: loans-service
    build: ../../loans
    ports:
      - 6091:6090
    depends_on:
      - mysqldb
      - configserver
      - eurekaserver
    environment:
      - spring.jpa.hibernate.ddl-auto=update
      - spring.datasource.url=jdbc:mysql://mysqldb:3306/test_ms_cloud
      - spring.datasource.username=root
      - spring.datasource.password=root
      - spring.datasource.driver-class-name=com.mysql.cj.jdbc.Driver
      - spring.application.name=LOANS-SERVICE
      - spring.profiles.active=default
      - spring.cloud.config.import=http://configserver:8071/
      - spring.cloud.config.import-check.enabled=false
      - eureka.client.registerWithEureka=true
      - eureka.client.fetchRegistry=true
      - eureka.client.serviceUrl.defaultZone=http://eurekaserver:8761/eureka/
      - management.zipkin.tracing.endpoint=http://zipkin:9411/api/v2/spans
    networks:
      - service-mysql-net
      - service-config-net
    deploy:
      restart_policy:
        condition: on-failure
        delay: 5s
        max_attempts: 3
        window: 120s

  prometheus:
    image: prom/prometheus:latest
    container_name: prometheus
    ports:
      - 9090:9090
    volumes:
      - ./prometheus.yml:/etc/prometheus/prometheus.yml
    networks:
      - service-config-net

  grafana:
    image: grafana/grafana:latest
    container_name: grafana
    ports:
      - 3000:3000
    environment:
      - GF_SECURITY_ADMIN_USER=admin
      - GF_SECURITY_ADMIN_PASSWORD=password
    networks:
      - service-config-net
    depends_on:
      - prometheus

  gateway-service:
    container_name: gateway-service
    build: ../../gatewayserver
    ports:
      - 9191:9191
    depends_on:
      - configserver
      - eurekaserver
      - accounts-service
      - cards-service
      - loans-service
    environment:
      - spring.application.name=API-GATEWAY-SERVICE
      - spring.profiles.active=default
      - spring.cloud.config.import=http://configserver:8071/
      - spring.cloud.config.import-check.enabled=false
      - spring.cloud.gateway.routes[0]_id=ACCOUNTS-SERVICE
      - spring.cloud.gateway.routes[0]_uri=lb://ACCOUNTS-SERVICE
      - spring.cloud.gateway.routes[0]_predicates=Path=/accounts/**
      #      - spring.cloud.gateway.routes[0].filters[0]=CircuitBreaker=name:ACCOUNTS,fallbackuri:forward:/accountServiceFallBack
      - spring.cloud.gateway.routes[1]_id=LOANS-SERVICE
      - spring.cloud.gateway.routes[1]_uri=lb://LOANS-SERVICE
      - spring.cloud.gateway.routes[1]_predicates=Path=/loans/**
      #      - spring.cloud.gateway.routes[1].filters[0]=CircuitBreaker=name:LOANS,fallbackuri:forward:/loanServiceFallBack
      - spring.cloud.gateway.routes[2]_id=CARDS-SERVICE
      - spring.cloud.gateway.routes[2]_uri=lb://CARDS-SERVICE
      - spring.cloud.gateway.routes[2]_predicates=Path=/cards/**
      #      - spring.cloud.gateway.routes[2].filters[0]=CircuitBreaker=name:CARDS,fallbackuri:forward:/cardServiceFallBack
      - eureka.client.registerWithEureka=true
      - eureka.client.fetchRegistry=true
      - eureka.client.serviceUrl.defaultZone=http://eurekaserver:8761/eureka/
      - management.zipkin.tracing.endpoint=http://zipkin:9411/api/v2/spans
    networks:
      - service-config-net
    deploy:
      restart_policy:
        condition: on-failure
        delay: 5s
        max_attempts: 3
        window: 120s


networks:
  service-mysql-net:
  service-config-net: