package com.example.gatewayserver.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FallBackMethodController {
    @GetMapping("/accountServiceFallBack")
    public String accountServiceFallBackMethod() {
        return "Account Service is taking longer than Expected." +
                " Please try again later";
    }

    @GetMapping("/cardServiceFallBack")
    public String cardServiceFallBackMethod() {
        return "Card Service is taking longer than Expected." +
                " Please try again later";
    }

    @GetMapping("/loanServiceFallBack")
    public String loanServiceFallBackMethod() {
        return "Loan Service is taking longer than Expected." +
                " Please try again later";
    }

}
