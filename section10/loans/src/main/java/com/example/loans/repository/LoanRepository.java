package com.example.loans.repository;

import java.util.List;

import com.example.loans.model.Loan;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LoanRepository extends CrudRepository<Loan, Integer> {

	
	List<Loan> findByCustomerIdOrderByStartDtDesc(int customerId);

}
