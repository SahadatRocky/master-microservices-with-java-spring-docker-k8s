package com.example.loans.model;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Customer {

	private int customerId;

}
