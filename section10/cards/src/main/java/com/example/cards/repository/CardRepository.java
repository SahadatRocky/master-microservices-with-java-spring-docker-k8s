package com.example.cards.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.cards.model.Card;

@Repository
public interface CardRepository extends CrudRepository<Card, Integer> {

	List<Card> findByCustomerId(Integer customerId);

}
