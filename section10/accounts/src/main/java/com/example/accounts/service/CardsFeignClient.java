package com.example.accounts.service;

import com.example.accounts.model.Card;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient(name = "CARDS-SERVICE")
public interface CardsFeignClient {

    @GetMapping("/cards/myCard/{customerId}")
    List<Card> getCardDetails(@PathVariable("customerId") Integer customerId);
}
