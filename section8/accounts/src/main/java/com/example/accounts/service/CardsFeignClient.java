package com.example.accounts.service;

import com.example.accounts.model.Card;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient("CARDS")
public interface CardsFeignClient {

    @GetMapping("/myCard/{customerId}")
    List<Card> getCardDetails(@PathVariable("customerId") Integer customerId);
}
