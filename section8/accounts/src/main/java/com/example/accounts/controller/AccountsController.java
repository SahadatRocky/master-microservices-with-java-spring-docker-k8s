package com.example.accounts.controller;

import com.example.accounts.config.AccountsServiceConfig;
import com.example.accounts.model.*;
import com.example.accounts.repository.AccountsRepository;
import com.example.accounts.service.CardsFeignClient;
import com.example.accounts.service.LoansFeignClient;
import com.example.accounts.service.MyApiService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.List;

@RestController
public class AccountsController {

    @Autowired
    private AccountsServiceConfig accountsConfig;
    @Autowired
    LoansFeignClient loansFeignClient;

    @Autowired
    CardsFeignClient cardsFeignClient;
    private final AccountsRepository accountsRepository;
    private final MyApiService apiService;

    public AccountsController(AccountsRepository accountsRepository,
                              MyApiService apiService){
        this.accountsRepository = accountsRepository;
        this.apiService = apiService;
    }

    @GetMapping("/myAccount/{id}")
    public Account getAccountDetails(@PathVariable("id") Integer id) {

        Account accounts = accountsRepository.findByCustomerId(id);
        if (accounts != null) {
            return accounts;
        } else {
            return null;
        }
    }

    @GetMapping("/account/properties")
    public String getPropertyDetails() throws JsonProcessingException {
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        Properties properties = new Properties(accountsConfig.getMsg(), accountsConfig.getBuildVersion(),
                accountsConfig.getMailDetails(), accountsConfig.getActiveBranches());
        String jsonStr = ow.writeValueAsString(properties);
        return jsonStr;
    }

    @GetMapping("/get-address/{latitude}/{longitude}")
    public Mono<JsonNode> fetchData(@PathVariable("latitude") String latitude, @PathVariable("longitude") String longitude ) {
        return apiService.fetchDataFromApi(latitude,longitude);
    }

    @GetMapping("/myCustomerDetails/{customerId}")
    public CustomerDetails myCustomerDetails(@PathVariable("customerId") Integer customerId) {
        Account accounts = accountsRepository.findByCustomerId(customerId);
        List<Loan> loans = loansFeignClient.getLoansDetails(customerId);
        List<Card> cards = cardsFeignClient.getCardDetails(customerId);

        CustomerDetails customerDetails = new CustomerDetails();
        customerDetails.setAccounts(accounts);
        customerDetails.setLoans(loans);
        customerDetails.setCards(cards);

        return customerDetails;

    }
}
