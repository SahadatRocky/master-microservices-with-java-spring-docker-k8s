package com.example.accounts.model;

import lombok.Data;
import java.util.List;

@Data
public class CustomerDetails {
    private Account accounts;
    private List<Loan> loans;
    private List<Card> cards;
}
