package com.example.accounts.service;

import com.example.accounts.model.Loan;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient(name = "LOANS-SERVICE")
public interface LoansFeignClient {

    @GetMapping( "/loans/myLoan/{customerId}")
    List<Loan> getLoansDetails(@PathVariable("customerId") Integer customerId);
}
